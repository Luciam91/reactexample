import React from 'react';
import axios from 'axios';
import Users from 'components/users';

class UserContainer extends React.Component {

  constructor(props) {
      super(props);
      let users = [];

      this.state = {
          users: users
      }
  }

  componentDidMount() {
      let _this = this;
      let url = 'http://jsonplaceholder.typicode.com/users';

      axios.get(url)
          .then(function(response){
              _this.setState({
                  users: response.data
              });
          });
  }


  render() {
    return (
        <div>
            <h1>User Count: {this.state.users.length}</h1>
            <Users users={this.state.users} />
        </div>
    );
  }
}

export default UserContainer;
