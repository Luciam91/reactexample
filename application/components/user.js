import React from 'react';

class Users extends React.Component {
    render() {
        return (
            <li key={this.props.user.id}>
                {this.props.user.name}
            </li>
        )
    }
}

export default Users;
