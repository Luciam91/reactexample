import React from 'react';
import User from 'components/user';

class Users extends React.Component {
    render() {
        return (
            <ul>
                {this.props.users.map(function(user, i){
                    return (
                        <User user={user} key={user.id} />
                    )
                })}
            </ul>
        )
    }
}

export default Users;
