import React from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory } from 'react-router';

// Layouts
import App from 'layout/app';

// Components
import UserContainer from 'components/user-container';
import World from 'components/world';


ReactDOM.render((
    <Router history={browserHistory}>
        <Route component={App}>
            <Route path="/" component={UserContainer} />
            <Route path="/world" component={World} />
        </Route>
    </Router>
), document.getElementById('application'));